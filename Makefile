all:
	mkdir -p build
	src/gen.py tests/rules2 -o build/parser-rule2.py
	src/gen.py tests/rules1 -o build/parser-rule1.py
	src/gen.py tests/rules3 -o build/parser-rule3.py
clean:
	rm -rf build
