# ael-parser

Ce projet a été réalisé par :
- Abir Benazzouz
- Adrien Bensiali
- Clémentine Bonneau
- Pierre Calmettes
- Clément Chaine
- Rayane Makri

## Génération du parser

Le parser peut être généré avec le makefile en utilisant la commande :
```bash
make
```

Deux parsers sont alors générés pour les règles fournies. Celles-ci sont disponibles dans le dossier `tests`.

## Utilisation du parser

Pour utiliser un parseur, il faut lancer la commande :
```bash
python3 <nom-du-parser> <mot>
```

## Fonctionnement du parser

Nous nous sommes inspiré du parser LR, le parser parcourt le mot de gauche à droite, pour chaque caractère on cherche toutes les règles qui ont le caractère au même indice. Nous avons utilisé un arbre composé de cellules. Chaque cellule est composée d'une cellule père, d'une valeur (un item) et des cellules fils.

Donc notre algorithme repose sur un parcourt d'arbre, au début toutes les règles issues de l'axiome sont ajoutées en subtree de la racine (la racine ayant donc comme père None, et comme valeur axiom). Pour chaque règle, si le premier caractère de la règle correspond au premier caractère du mot on développe la branche. Sinon si le caractère suivant de la règle est un non terminal, on remplace dans l'arbre le  noeud de la règle par des noeuds qui remplace le non terminal par les règles qu'il donne. On avance dans le mot est dans la règle. Si au final on n'arrive pas à retrouver le mot on remonte la branche et on teste avec les autres noeuds non parcourus. Si on arrive à un noeud qui est à une profondeur égale à la taille du mot, on vérifie que le mot est égale à la valeur du noeud, oui : on accepte le mot, non : le mot est refusé. Si on est à la règle la plus à droite dans l'arbre, le noeud père est donc supprimé car aucune règle qu'il engendre n'est bonne, on remonte donc de deux dans l'arbre en supprimant le noeud. Si par contre on est à la règle la plus à droite et que le père est le noeud de l'axiome, alors le mot est refusé

## Tests unitaires

Nous avons testé notre generateur de parser avec trois grammaires différentes, les deux que vous nous avez proposé en mail plus une grammaire simple contenant un non terminal à gauche dans la règle de l'axiome. Nous avons testé les parser générés avec des mots appartenants aux langages et des mots ni appartenant pas.
Notre tests étaient concluants. 

