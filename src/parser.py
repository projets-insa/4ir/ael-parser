# grammar

import sys

if len(sys.argv) != 2:
    print("Usage: " + sys.argv[0] + " mot")
    sys.exit(1)

class Cell:
    def __init__(self, parent, value, sub):
        self.parent = parent
        self.value = value
        self.sub = sub

input = sys.argv[1]
input = input.split()


# recover the axiom
axiom = grammar[0][0]
arbre = Cell(None, axiom, None)
values = []
for r in grammar:
    if r[0] == axiom:
        values += [Cell(arbre, r[1], [])]
arbre.sub = values

#Si les règles de l'axiome commencent par un non terminal il faut

# profondeur
def check_cell(p, subtree):
    
    # Si on atteint la profondeur de la taille du mot, on a fini
    if p == len(input):
        if subtree != [] and input == subtree[0].value:
            print("OK")
            exit(0)
        else:
            print("KO")
            exit(0)
    elif len(subtree) > 0:
        c = subtree[0]
        x = True

        #Si la règle contient un espace car il y a une epsilone-transition dans la grammaire, ce if permet de retirer l'espace
        if p < len(c.value) and c.value[p]==' ':
            c.value.remove(' ')

        #On regarde si le prochain caractère dans la règle est un non terminal si on est pas a la fin de la règle
        if p < len(c.value):
            for r in grammar:
                if r[0] == c.value[p]:
                    x = False
                    if p == 0:
                        c.sub += [Cell(c.parent,r[1] + c.value[1:],[])]
                    else:
                        c.sub += [Cell(c.parent,c.value[0:p] + r[1] + c.value[p+1:],[])]

            if x == False:            
                c.parent.sub.remove(c)
                c.parent.sub += c.sub
                check_cell(p, c.parent.sub)
        
 
        #le caractère du mot à l'indice p correspond à celui de la règle
        if x == True and  p < len(c.value) and input[p] == c.value[p]:
        # On construit cell.sub
            c.sub =  [Cell(c,c.value,[])]
            check_cell(p+1, c.sub)

        #le caractère ne correspond pas et on est à la dernière règle du père
        elif subtree.index(c) == len(subtree)-1:
            #le parent est l'axiome, toutes les règles ont été testées
            if   c.parent.parent == None or c.parent.parent.value == axiom and c.parent.parent.sub.index(c.parent) == len(c.parent.parent.sub)-1:
                print("ko")
                exit(1)
            else:
                """
                if c.parent.parent != None and c.parent.parent.parent != None:
                    print(c.parent.parent.parent.value)
                    """
                next = []
                while next == [] and c.parent.parent != None:
                    c.parent.parent.sub.remove(c.parent)
                    next = c.parent.parent.sub
                    c = c.parent
                    p = p-1
                
                #on est remonté tout en haut et il n'y a plus de sub à analyser, donc le mot est refusé
                if next == []:
                    print("ko")
                    exit(1)
                check_cell(p, next)

        #le caractère ne correspond pas mais on n'est pas à la dernière règle
        else:
            subtree.remove(c)
            check_cell(p,subtree)

            

check_cell(0, arbre.sub)
                    
