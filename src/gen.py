#!/usr/bin/python3

import argparse, sys

def grammar_to_string(grammar):
    out = "grammar = ["
    for r in grammar:
        out += "[\"" + r[0] + "\", ["
        for right in r[1]:
            out += "\"" + right + "\","
        out += "]],"
    out += "]"
    return out

def read_grammar(filename):
    f = open(filename)
    grammar = []
    for line in f:
        line = line.split()
        print(line[2:])
        if line[2:] == []:
            grammar += [[line[0]] + [[" "]]]
        else:
            grammar += [[line[0]] + [line[2:]]]
            
    f.close()
    return grammar

def usage():
    print("usage")

def main(argv):
    parser = argparse.ArgumentParser()
    parser.add_argument('-o', '--output')
    parser.add_argument('grammar', nargs=1)
    args = parser.parse_args()
        
    # only one as defined by the argument
    grammar = args.grammar[0]
    output = args.output

    # convert test grammar to table
    grammar = read_grammar(grammar)
    # convert table to a string python table
    grammar = grammar_to_string(grammar)
    
    # Append the parser to the grammar
    parser = grammar + "\n\n"
    f = open("src/parser.py")
    for l in f:
        parser += l
    f.close()

    # Write the parser to the output
    f = open(output, "w")
    f.write(parser)

if __name__ == "__main__":
    main(sys.argv)
